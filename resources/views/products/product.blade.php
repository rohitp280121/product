<!DOCTYPE html>
<html>

<head>
    <title>Product Model</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>


</head>

<body>

    <div class="container">
        <h1>Product Model</h1>
        <a class="btn btn-success" href="javascript:void(0)" id="createNewProduct"> Create New Product</a>
        <table class="table table-bordered data-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Code</th>
                    <th width="280px">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        id="ajaxModel" aria-hidden="true">

        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading"></h4>
                </div>
                <div class="modal-body">
                    <form id="productForm" name="productForm" enctype="multipart/form-data" class="form-horizontal">
                        <input type="hidden" name="product_id" id="product_id">

                        <div class="form-group">


                            <label for="name" class="control-label">Category</label>
                            <select class="form-control select2bs4" data-placeholder="Select Category" tabindex="1"
                                name="cat_id" id="cat_id">
                                <option value="">Select Category</option>
                                @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->cat_name }}</option>
                                @endforeach
                            </select>
                            <span class="text-danger">
                                <strong id="cat-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">


                            <label for="name" class="control-label">SubCategory</label>
                            <select class="form-control select2bs4" data-placeholder="Select SubCategory" tabindex="1"
                                name="sub_cat_id" id="sub_cat_id">
                                <option value="">Select SubCategory</option>
                                @foreach ($subcategories as $subcategory)
                                <option value="{{ $subcategory->id }}">{{ $subcategory->sub_cat_name }}</option>
                                @endforeach
                            </select>
                            <span class="text-danger">
                                <strong id="subcat-error"></strong>
                            </span>
                        </div>


                        <div class="form-group">


                            <label for="name" class="control-label">Product Status</label>
                            <select class="form-control select2bs4" data-placeholder="Select Product Status" tabindex="1"
                                name="product_status" id="product_status">
                                <option value="">Select Prduct Status</option>
                                <option value="Inactive">Inactive</option>
                                <option value="Active">Active</option>
                            </select>
                        </div>

                        <div class="form-group">


                            <label for="name" class="control-label">Product Name</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="product_name" name="product_name"
                                    placeholder="Enter Name" value="" maxlength="50" required="">
                            </div>
                            <span class="text-danger">
                                <strong id="name-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">

                            <label class="control-label">Product Code</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="product_code" name="product_code"
                                    placeholder="Enter Code" value="" maxlength="50" required="">
                            </div>
                            <span class="text-danger">
                                <strong id="code-error"></strong>
                            </span>
                        </div>


                        <div class="form-group">
                            <label class="control-label">Product Description</label>
                            <div class="col-sm-12">
                                <textarea id="product_description" name="product_description" required=""
                                    placeholder="Enter Description" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Product Image</label>
                            <div class="col-sm-12">
                                <img id="product_image" hidden width="250" height="250">
                                <input id="product_image_value" type="hidden" name="product_image_value">
                                <input type="file" class="form-control" id="product_url" name="product_url" required="">
                            </div>
                        </div>

                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>

<script type="text/javascript">
    $(function () {

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('products.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'product_name', name: 'product_name'},
            {data: 'product_code', name: 'product_code'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    $('#createNewProduct').click(function () {
        $('#saveBtn').val("create-product");
        $('#product_id').val('');
        $('#productForm').trigger("reset");
        $('#modelHeading').html("Create New Product");
        $('#ajaxModel').modal('show');
    });

    $('body').on('click', '.editProduct', function () {
        var base_url = "{{asset('/')}}";
      var product_id = $(this).data('id');
      $('#product_image').removeAttr('hidden');
      $.get("{{ route('products.index') }}" +'/' + product_id +'/edit', function (data) {
          $('#modelHeading').html("Edit Product");
          $('#saveBtn').val("edit-user");
          $('#ajaxModel').modal('show');
          $('#product_id').val(data.id);
          $('#product_name').val(data.product_name);
          $('#product_code').val(data.product_code);
          $('#cat_id').val(data.cat_id);
          $('#sub_cat_id').val(data.sub_cat_id);
          $('#product_status').val(data.product_status);
          $('#product_image').prop('src', base_url +'public/image/'+data.product_url);
          $('#product_description').val(data.product_description);
          $('#product_image_value').val(data.product_url);



      })
   });

    $('#saveBtn').click(function (e) {
        e.preventDefault();
        // $(this).html('Sending..');


        var formData = new FormData();

        var file_data = $('#product_url').prop('files')[0];
        var form_data = new FormData();
        form_data.append('product_url', file_data);
        form_data.append('product_id', $('#product_id').val());
        form_data.append('product_code', $('#product_code').val());
        form_data.append('product_name', $('#product_name').val());
        form_data.append('cat_id', $('#cat_id').val());
        form_data.append('sub_cat_id', $('#sub_cat_id').val());
        form_data.append('product_description', $('#product_description').val());
        form_data.append('product_image_value', $('#product_image_value').val());
        form_data.append('product_status', $('#product_status').val());




        $.ajax({
         data: form_data,
        contentType: false,
        processData: false,
          url: "{{ route('products.store') }}",
          type: "POST",
          enctype: 'multipart/form-data',
          dataType: 'json',
          success: function (data) {
            $( '#cat-error' ).html( "" );
        $( '#subcat-error' ).html( "" );
        $( '#name-error' ).html( "" );
        $( '#code-error' ).html( "" );
        if(data.errors) {
                    if(data.errors.cat_id){
                        $( '#cat-error' ).html( data.errors.cat_id[0] );
                    }
                    if(data.errors.sub_cat_id){
                        $( '#subcat-error' ).html( data.errors.sub_cat_id[0] );
                    }
                    if(data.errors.product_name){
                        $( '#name-error' ).html( data.errors.product_name[0] );
                    }
                    if(data.errors.product_code){
                        $( '#code-error' ).html( data.errors.product_code[0] );
                    }

                }
                else{
                    $('#productForm').trigger("reset");
              $('#ajaxModel').modal('hide');
              table.draw();
              $('#saveBtn').html('Save Changes');

                }

          }
      });
    });

    $('body').on('click', '.deleteProduct', function () {

        var product_id = $(this).data("id");
        confirm("Are You sure want to delete !");

        $.ajax({
            type: "DELETE",
            url: "{{ route('products.store') }}"+'/'+product_id,
            success: function (data) {
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

  });
</script>

</html>
