<!DOCTYPE html>
<html>

<head>
    <title>Product Model</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
</head>

<body>

    <div class="container">
        <h1>Product Model</h1>
        <a class="btn btn-success" href="javascript:void(0)" id="createNewProduct"> Create New Option Price</a>
        <table class="table table-bordered data-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Option Name</th>
                    <th> Option Price</th>
                    <th width="280px">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        id="ajaxModel" aria-hidden="true">

        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading"></h4>
                </div>
                <div class="modal-body">
                    <form id="productForm" name="productForm" enctype="multipart/form-data" class="form-horizontal">
                        <input type="hidden" name="option_id" id="option_id">

                        <div class="form-group">
                            <label for="name" class="control-label">Product</label>
                            <select class="form-control select2bs4" data-placeholder="Select Product" tabindex="1"
                                name="product_id" id="product_id">
                                <option value="">Select Product</option>
                                @foreach ($products as $product)
                                <option value="{{ $product->id }}">{{ $product->product_name }}</option>
                                @endforeach
                            </select>
                            <span class="text-danger">
                                <strong id="product-error"></strong>
                            </span>
                        </div>



                        <div class="form-group">
                            <label for="name" class="control-label">Option Name</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="option_name" name="option_name"
                                    placeholder="Enter Option Name" value="" maxlength="50" required="">
                            </div>
                            <span class="text-danger">
                                <strong id="name-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">

                            <label class="control-label">Option Price</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="option_price" name="option_price"
                                    placeholder="Enter Option Price" onkeypress="return isNumber(event)" value="" maxlength="50" required="">
                            </div>
                            <span class="text-danger">
                                <strong id="price-error"></strong>
                            </span>
                        </div>

                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>

<script type="text/javascript">
    $(function () {

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('product_options.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'option_name', name: 'option_name'},
            {data: 'option_price', name: 'option_price'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    $('#createNewProduct').click(function () {
        $('#saveBtn').val("create-product");
        $('#option_id').val('');
        $('#productForm').trigger("reset");
        $('#modelHeading').html("Create New Product Option");
        $('#ajaxModel').modal('show');
    });

    $('body').on('click', '.editProduct', function () {
        var base_url = "{{asset('/')}}";
      var product_id = $(this).data('id');
      $('#product_image').removeAttr('hidden');
      $.get("{{ route('product_options.index') }}" +'/' + product_id +'/edit', function (data) {
          $('#modelHeading').html("Edit Product Option");
          $('#saveBtn').val("edit-user");
          $('#ajaxModel').modal('show');
          $('#option_id').val(data.id);
          $('#option_name').val(data.option_name);
          $('#option_price').val(data.option_price);
          $('#product_id').val(data.product_id);
      })
   });

    $('#saveBtn').click(function (e) {
        e.preventDefault();
        var form_data = new FormData();
        form_data.append('option_id', $('#option_id').val());
        form_data.append('option_name', $('#option_name').val());
        form_data.append('option_price', $('#option_price').val());
        form_data.append('product_id', $('#product_id').val());



        $.ajax({
         data: form_data,
        contentType: false,
        processData: false,
          url: "{{ route('product_options.store') }}",
          type: "POST",
          enctype: 'multipart/form-data',
          dataType: 'json',
          success: function (data) {
         $('#product-error' ).html( "" );
        $('#price-error' ).html( "" );
        $('#name-error' ).html( "" );
        if(data.errors) {
                    if(data.errors){
                        $('#product-error' ).html( data.errors.product_id[0] );
                    }
                    if(data.errors.price){
                        $('#price-error' ).html( data.errors.option_price[0] );
                    }
                    if(data.errors.product_name){
                        $('#name-error' ).html( data.errors.option_name[0] );
                    }


                }
                else{
                    $('#productForm').trigger("reset");
              $('#ajaxModel').modal('hide');
              table.draw();
              $('#saveBtn').html('Save Changes');

                }

          }
      });
    });

    $('body').on('click', '.deleteProduct', function () {

        var product_id = $(this).data("id");
        confirm("Are You sure want to delete !");

        $.ajax({
            type: "DELETE",
            url: "{{ route('product_options.store') }}"+'/'+product_id,
            success: function (data) {
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

  });

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>

</html>
