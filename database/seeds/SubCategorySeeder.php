<?php

use Illuminate\Database\Seeder;
use App\Sub_category;

class SubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Sub_category::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $sub_category = [
            ['id' => 1, 'sub_cat_name' => 'Animal subcategory', 'sub_cat_description' => 'Animal subcategory details', 'sub_cat_image' => 'https://i.picsum.photos/id/237/200/300.jpg?hmac=TmmQSbShHz9CdQm0NkEjx1Dyh_Y984R9LpNrpvH2D_U','cat_id'=>1],
            ['id' => 2, 'sub_cat_name' => 'Book subcategory', 'sub_cat_description' => 'Book subcategory details', 'sub_cat_image' => 'https://i.picsum.photos/id/119/3264/2176.jpg?hmac=PYRYBOGQhlUm6wS94EkpN8dTIC7-2GniC3pqOt6CpNU','cat_id'=>2],
        ];

        Sub_category::insert($sub_category);
    }
}
