<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $fillable = [
        'cat_id',
        'sub_cat_id',
        'product_name',
        'product_description',
        'product_code',
        'product_url',
        'product_status'
    ];

    public function ProductOption()
    {
    	return $this->hasOne(product_option::class);
    }
}
