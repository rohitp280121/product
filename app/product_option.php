<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product_option extends Model
{
    protected $fillable = [
        'product_id',
        'option_name',
        'option_price'
    ];

    public function product()
    {
    	return $this->belongsTo(product::class,'product_id');
    }
}
