<?php

namespace App\Http\Controllers;
use App\product_option;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Validator;
use App\product;
class ProductOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = product::latest()->get();
        if ($request->ajax()) {
            $data = product_option::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct">Edit</a>';

                    $btn = $btn . ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct">Delete</a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('products.productOption', compact('products', 'product_option'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|exists:products,id',
            'option_price' => 'required',
            'option_name' => 'required',
        ]);
        if ($validator->passes()) {
            product_option::updateOrCreate(
                ['id' => $request->option_id],
                [
                    'option_price' => $request->option_price,
                    'option_name' => $request->option_name,
                    'product_name' => $request->product_name,
                    'product_id' => $request->product_id,
                ]
            );

            return response()->json(['success' => 'Product Option saved successfully.']);
        } else {
            return response()->json(['errors' => $validator->errors()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\product_option  $product_option
     * @return \Illuminate\Http\Response
     */
    public function show(product_option $product_option)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\product_option  $product_option
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = product_option::find($id);
        return response()->json($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\product_option  $product_option
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, product_option $product_option)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\product_option  $product_option
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        product_option::find($id)->delete();

        return response()->json(['success' => 'Product Option deleted successfully.']);
    }
}
