<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\product;
use Illuminate\Http\Request;
use DataTables;
use App\Category;
use App\Sub_category;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::latest()->get();
        $subcategories = Sub_category::latest()->get();
        if ($request->ajax()) {
            $data = Product::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct">Edit</a>';

                    $btn = $btn . ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct">Delete</a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('products.product', compact('products', 'categories', 'subcategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'cat_id' => 'required|exists:categories,id',
            'sub_cat_id' => 'required|exists:sub_categories,id',
            'product_name' => 'required',
            'product_code' => 'required',
        ]);
        if ($validator->passes()) {

            $filename = '';
            if ($request->file('product_url')) {
                $file = $request->file('product_url');
                $filename = date('YmdHi') . $file->getClientOriginalName();
                $file->move(public_path('public/Image'), $filename);
            }
            Product::updateOrCreate(
                ['id' => $request->product_id],
                [
                    'cat_id' => $request->cat_id,
                    'product_url' =>  $filename==''?$request->product_image_value:$filename,
                    'sub_cat_id' => $request->sub_cat_id,
                    'product_name' => $request->product_name,
                    'product_code' => $request->product_code,
                    'product_status' => $request->product_status,
                    'product_description' => $request->product_description
                ]
            );

            return response()->json(['success' => 'Product saved successfully.']);
        } else {
            return response()->json(['errors' => $validator->errors()]);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return response()->json($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();

        return response()->json(['success' => 'Product deleted successfully.']);
    }
}
