<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_category extends Model
{
    protected $fillable = [
        'sub_cat_name',
        'sub_cat_description',
        'sub_cat_image',
        'cat_id'
    ];

    public function category()
    {
    	return $this->belongsTo(Category::class,'cat_id');
    }
}
