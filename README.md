## Product Management System

Steps For Installation for Project

1)git clone https://gitlab.com/rohitp280121/product.git

In this step we are create new .env file from example.env
we can use .env file for database connection and email configuration setting etc.


2)
i) cd loan-application
ii) cp .env.example .env

In this step we are install the application laravel packages
3) composer install

In this step we are genrated a application key for project.
4)php artisan key:generate

Here we migrate all our migration to our database 
5)
i)php artisan migrate

Here we seed  details to our  table 
6)php artisan db:seed

For External package install use below command which is use for datatable

composer require yajra/laravel-datatables-oracle


